import React, { useState, useEffect } from "react";
import StarRatings from "react-star-ratings";
import { axiosInstance } from "../../components/api/axios";
import Link from "next/link";
import WordLimit from "react-word-limit";
import Head from "next/head";

import { FaSearchPlus } from "react-icons/fa";
function index({ products, count }) {
  return (
    <>
      <Head>
        <title>Products - Implant Reviews</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="section-pagetop bg">
        <div className="container">
          <h2 className="title-page">All Products</h2>
          <nav>
            <ol className="breadcrumb text-white">
              <li className="breadcrumb-item">
                <a href="#">Home</a>
              </li>
              <li className="breadcrumb-item">
                <a href="#">All Products</a>
              </li>
            </ol>
          </nav>
        </div>
      </section>

      <section className="section-content padding-y">
        <div className="container">
          <div className="row">
            <main className="col">
              <header className="border-bottom mb-4 pb-3">
                <div className="form-inline">
                  <span className="mr-md-auto">{count} Items found </span>
                </div>
              </header>

              <div className="row">
                {products.map((product) => (
                  <Link key={product.id} href={`/product/${product.slug}`}>
                    <div className="col-md-4">
                      <figure className="card card-product-grid">
                        <div className="img-wrap">
                          <img
                            src={`${process.env.API_URL}${product.images[0].url}`}
                          />
                          <a className="btn-overlay" href="#">
                            <FaSearchPlus /> Quick view
                          </a>
                        </div>
                        <figcaption className="info-wrap">
                          <div className="fix-height">
                            <a href={`/product/${product.slug}`}>
                              <h6 className="title">
                                <WordLimit limit={25}>
                                  {product.productname}
                                </WordLimit>
                              </h6>
                            </a>
                            <p>
                              <WordLimit limit={25}>
                                {product.manufacturer}
                              </WordLimit>
                            </p>
                            <div className="price-wrap my-2">
                              <h4 className="price">₹{product.price}</h4>
                            </div>
                          </div>
                        </figcaption>
                      </figure>
                    </div>
                  </Link>
                ))}
              </div>
            </main>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps() {
  const r = await axiosInstance.get("/products");
  const products = r.data;
  return {
    props: { products, count: products.length },
  };
}

export default index;
