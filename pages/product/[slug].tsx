import { useState, useEffect } from "react";
import Head from "next/head";
import { axiosInstance } from "../../components/api/axios";
import StarRatings from "react-star-ratings";
import ImageGallery from "react-image-gallery";
import { parseCookies } from "nookies";
import { FaStar } from "react-icons/fa";

import format from "date-fns/format";
function ProductPage({ product, reviews, setTab, setModal }) {
  const [comment, setComment] = useState("");
  const [starratings, setStarratings] = useState(0);
  const [isAuthenticated, setisAuthenticated] = useState(false);
  const [avg, setAvg] = useState(0);
  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const getSigned = () => {
    setisAuthenticated(!!parseCookies().token as any);
  };
  useEffect(() => {
    getSigned();
    avgratings();
  }, [isAuthenticated]);
  const handleSubmit = async (e) => {
    e.preventDefault();
    const headers = {
      headers: {
        Authorization: `Bearer ${parseCookies().token}`,
      },
    };
    const jwtvalidate = await axiosInstance.get("/users/me", headers);
    const user = jwtvalidate.data;

    if (comment !== "" && starratings !== 0 && user.id !== null) {
      const prodid = product.id;

      const data = {
        comment,
        starratings,
        product: prodid,
      };
      const headers = {
        headers: {
          Authorization: `Bearer ${parseCookies().token}`,
        },
      };
      await axiosInstance
        .post(`/reviews`, data, headers)
        .then((r) => {
          if (r.status === 200) {
            setMessage("Thank you for your feedback");
            setSuccess(true);
            setTimeout(() => {
              setSuccess(false);
              setMessage("");
            }, 2000);
          } else {
            setMessage("Something went wrong");
            setSuccess(false);
          }
        })
        .catch((e) => console.table(e.config));

      setComment("");
      setStarratings(0);
    }
  };
  const handleChange = (value: any) => {
    setStarratings(value);
  };

  const avgratings = () => {
    let arr = [];
    if (reviews) {
      Object.values(reviews).map((m) => {
        arr.push(m["starratings"]);
      });
      if (arr !== []) {
        let sum = arr.reduce((a, b) => (a += b));
        setAvg(Number((sum / arr.length).toFixed(2)));
      }
    }
  };

  return (
    <>
      <Head>
        <title>{product?.productname} - Implant Reviews</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {product.id === undefined ? (
        "loading"
      ) : (
        <div className="container mt-5 mb-2 border-0">
          <div className="row no-gutters">
            <aside className="col-md-6">
              <ImageGallery
                showFullscreenButton={false}
                showPlayButton={false}
                items={product.images.map((i: any) => {
                  return {
                    original: `${process.env.API_URL}${i.url}`,
                    thumbnail: `${process.env.API_URL}${i.url}`,
                  };
                })}
              />
            </aside>
            <main className="col-md-6 border-left">
              <article className="content-body">
                <h2 className="title">{product.productname}</h2>

                <div className="rating-wrap my-3">
                  <StarRatings
                    starRatedColor="#ffdb0f"
                    starDimension="1.5em"
                    starSpacing="0.3em"
                    rating={avg}
                  />
                  <small className="label-rating text-muted">
                    {reviews.length} reviews
                  </small>
                </div>

                <div className="mb-3">
                  <var className="price h4">₹{product.price}</var>
                </div>

                <p>{product.description}</p>

                <hr />
              </article>
            </main>
          </div>
        </div>
      )}
      <hr className="mx-5" />
      <div className="container my-5">
        <header className="section-heading m-4">
          <h3>Reviews</h3>
          <div className="rating-wrap ">
            <ul className="rating-stars stars-lg"></ul>
            <strong className="label-rating text-lg">
              {avg}
              <FaStar style={{ color: "#ffdb0f", marginBottom: "5px" }} />{" "}
              <span className="text-muted">| {reviews.length} reviews</span>
            </strong>
          </div>
        </header>

        {reviews?.map((r: any) => (
          <article key={r.id} className="box mb-3">
            <div className="icontext w-100">
              <img
                src="https://www.w3schools.com/howto/img_avatar.png"
                className="img-xs icon rounded-circle"
              />

              <div className="text">
                <span className="date text-muted float-md-right">
                  {format(new Date(r.published_at), "dd MMM yy")}
                </span>
                <h6 className="mb-1">
                  {r.users_permissions_user?.username || "Anomymous User"}
                </h6>
                <StarRatings
                  starRatedColor="#ffdb0f"
                  starDimension="1em"
                  starSpacing="0.3em"
                  starHoverColor={"#DAA520"}
                  rating={r.starratings}
                />
              </div>
            </div>{" "}
            <div className="mt-3 d-flex">
              <p>{r.comment}</p>
            </div>
          </article>
        ))}
        <section className="container mt-5">
          <p className="lead">
            Please leave a review to help others make a decision
          </p>
          {isAuthenticated ? (
            <div>
              <form>
                <div className="col-md-6 col-sm-12">
                  <div className="row">
                    <StarRatings
                      starDimension="2em"
                      starSpacing="0.3em"
                      starHoverColor={"#DAA520"}
                      starRatedColor={"#F8B704"}
                      rating={starratings}
                      changeRating={handleChange}
                    />
                  </div>
                  <div className="row mt-3">
                    <textarea
                      rows={4}
                      className="form-control"
                      placeholder={"What do you think about this product?"}
                      value={comment}
                      onChange={(e) => setComment(e.target.value)}
                    />
                    {message ? (
                      <>
                        <div
                          className={`p-3 m-2 alert 
                         ${success ? "alert-success" : "alert-danger"}
                        `}
                        >
                          {message}
                        </div>
                      </>
                    ) : (
                      <></>
                    )}
                    <button
                      className="my-3 btn btn-outline-primary"
                      type="submit"
                      onClick={handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </form>{" "}
            </div>
          ) : (
            <div className="row">
              <div className="col">
                <button
                  onClick={() => {
                    setTab(1);
                    setModal(true);
                  }}
                  type="button"
                  className="btn btn-outline-primary mr-2"
                >
                  Sign in
                </button>
                <button
                  onClick={() => {
                    setTab(2);
                    setModal(true);
                  }}
                  type="button"
                  className="btn btn-primary"
                >
                  Sign up
                </button>
              </div>
            </div>
          )}
        </section>
      </div>
    </>
  );
}
export async function getStaticPaths() {
  const allProductsResponse = await axiosInstance.get("/products");
  const allProducts = allProductsResponse.data;
  const paths = allProducts.map((product: any) => ({
    params: { slug: product.slug },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const productResponse = await axiosInstance.get(`/products/${params.slug}`);
  const product = productResponse.data;
  const reviewsResponse = await axiosInstance.get(
    `/reviews?product.id=${product.id}`
  );
  const reviews = reviewsResponse.data;

  return {
    props: {
      product,
      reviews,
    },
  };
}

export default ProductPage;
