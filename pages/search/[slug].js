import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { FaSearchPlus } from "react-icons/fa";
import { axiosInstance } from "../../components/api/axios";
import Head from "next/head";
import WordLimit from "react-word-limit";
import Link from "next/link";
function SearchPage() {
  let router = useRouter();
  const [product, setProduct] = useState([]);
  const { slug } = router.query;
  const fetchData = async () => {
    await axiosInstance
      .get(`/products?productname_contains=${slug}`)
      .then((res) => {
        setProduct(res.data);
        console.log(res);
      })
      .catch((err) => console.error(err));
  };
  useEffect(() => {
    if (!router.isReady) {
      return;
    } else {
      fetchData();
    }
  }, [router.isReady]);
  return (
    <>
      <Head>
        <title>Search - Implant Reviews</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container">
        {!product.length ? (
          <h4 className="m-3">No products found</h4>
        ) : (
          <>
            {" "}
            <h4 className="m-3">Found {product.length} items</h4>
            <div className="row m-3">
              {product.map((product) => (
                <Link key={product.id} href={`/products/${product.slug}`}>
                  <div className="col-md-4">
                    <figure className="card card-product-grid">
                      <div className="img-wrap">
                        <img
                          src={`${process.env.API_URL}${product.images[0].url}`}
                        />
                        <a className="btn-overlay" href="#">
                          <FaSearchPlus /> Quick view
                        </a>
                      </div>
                      <figcaption className="info-wrap">
                        <div className="fix-height">
                          <h6 href="#" className="title">
                            <WordLimit limit={25}>
                              {product.productname}
                            </WordLimit>
                          </h6>
                          <p>
                            <WordLimit limit={25}>
                              {product.manufacturer}
                            </WordLimit>
                          </p>
                          <div className="price-wrap my-2">
                            <h4 className="price">${product.price}</h4>
                            {/* <del className="price-old">$1980</del> */}
                          </div>
                        </div>
                        {/* <a href="#" className="btn btn-block btn-primary">
                          Add to cart{" "}
                        </a> */}
                      </figcaption>
                    </figure>
                  </div>
                </Link>
              ))}
            </div>
          </>
        )}
      </div>
    </>
  );
}

export default SearchPage;
