import Head from "next/head";
import Products from "../components/products";
import { FaArrowRight } from "react-icons/fa";
import { axiosInstance } from "../components/api/axios";

export default function Home({ categories }) {
  return (
    <>
      <Head>
        <title>Implant Reviews | Best Implants for Your Patients</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-md-6 col-sm-12">
            <h1 className="display-4 pt-3 pl-3">
              Choose the best products for your patients
            </h1>
          </div>
          <div className="col-md-6 col-sm-12">
            <img
              src={require("./cover.svg")}
              className="img-fluid"
              alt="Surgeons in Operation Theatre"
            />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="mt-5" id="ourproducts">
          <h2 className="max-w-lg mb-6 font-sans text-2xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl ml-4">
            Recent Products
          </h2>
        </div>
        <Products categories={categories} />
      </div>

      <div className="container my-3">
        <div className="row">
          <div className="col jurty-content-end">
            <a className="float-right" href="/products">
              <h5>
                Explore all products <FaArrowRight />
              </h5>
            </a>
          </div>
        </div>
      </div>
    </>
  );
}

export async function getStaticProps() {
  const r = await axiosInstance.get("/categories");
  const categories = r.data;
  return {
    props: { categories },
  };
}
