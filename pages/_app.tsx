import "../styles/globals.css";
import "../styles/bootstrap.css";
import "../styles/responsive.css";
import "../styles/style.css";
import "../styles/ui.css";
import Header from "../components/header";
import Footer from "../components/footer";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import * as gtag from "../lib/gtag";
function MyApp({ Component, pageProps }) {
  const [modal, setModal] = useState(false);
  const [tab, setTab] = useState(1);
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  return (
    <>
      <Header tab={tab} setTab={setTab} modal={modal} setModal={setModal} />
      <Component
        tab={tab}
        setTab={setTab}
        modal={modal}
        setModal={setModal}
        {...pageProps}
      />
      <Footer />
    </>
  );
}

export default MyApp;
