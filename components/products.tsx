import { FiArrowRightCircle, FiArrowLeftCircle } from "react-icons/fi";
import ScrollMenu from "react-horizontal-scrolling-menu";
import React from "react";
import WordLimit from "react-word-limit";

export default function Products({ categories }) {
  return (
    <div className="card card-body">
      {categories &&
        categories.map((cat: any) => (
          <React.Fragment key={cat.id}>
            <h4>{cat.title}</h4>
            <ScrollMenu
              alignCenter={false}
              hideArrows={true}
              hideSingleArrow={true}
              arrowLeft={<FiArrowLeftCircle style={{ fontSize: "40px" }} />}
              arrowRight={<FiArrowRightCircle style={{ fontSize: "40px" }} />}
              data={cat.products.map((product: any) => (
                <a key={product.id} href={`/product/${product.slug}`}>
                  <figure className="card-product-grid mb-0 card-sm">
                    <div className="img-wrap prod-img">
                      <img
                        src={`${process.env.API_URL}${product.images[0].url}`}
                      />
                    </div>
                    <figcaption className="info-wrap text-left">
                      <h5 className="title">
                        <WordLimit limit={15}>{product.productname}</WordLimit>
                      </h5>

                      <p className="mt-2">
                        <WordLimit limit={15}>{product.manufacturer}</WordLimit>
                      </p>

                      <h5 className="mt-2">₹ {product.price}</h5>
                    </figcaption>
                  </figure>
                </a>
              ))}
            />
          </React.Fragment>
        ))}
    </div>
  );
}
