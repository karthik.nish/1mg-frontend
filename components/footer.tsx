import React from "react";
import Link from "next/link";

function footer() {
  return (
    <footer className="section-footer border-top">
      <div className="container">
        <section className="footer-top padding-y">
          <div className="row">
            <aside className="col-12 col-md-8">
              <article className="mr-3">
                <h6 className="title">Our philosophy</h6>
                <p className="mt-3">
                  At Implant Reviews, we believe in providing unbiased
                  information about the latest surgical products. All
                  information is verified and created by doctors for other
                  doctors.
                </p>
              </article>
            </aside>
            <aside className="col-12 col-md-4 mt-5 mt-md-0">
              <h6 className="title">Quick Links</h6>
              <ul className="list-unstyled">
                <li>
                  <a href="/">Home</a>
                </li>

                <li>
                  <a href="/products">Products</a>
                </li>
                {/* <li>
                  <a href="#">About us</a>
                </li>
                <li>
                  <a href="#">Contact</a>
                </li> */}
              </ul>
            </aside>
          </div>
        </section>
        <section className="footer-copyright border-top">
          <p className="text-muted">
            2021 Implants Review © All rights reserved{" "}
          </p>
        </section>
      </div>
    </footer>
  );
}

export default footer;
