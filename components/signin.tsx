import { useState } from "react";
import { FaGoogle, FaEnvelope, FaLock } from "react-icons/fa";
import router from "next/router";
import { setCookie } from "nookies";
import GoogleLogin from "react-google-login";
import { axiosInstance } from "./api/axios";
function Signin({ setModal }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();

    try {
      const response = await fetch(`${process.env.API_URL}/auth/local/`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          identifier: email,
          password,
        }),
      });

      const data = await response.json();
      console.log("data", data);

      if (data.message) {
        setError(data.message[0].messages[0].message);

        return;
      }
      setCookie(null, "token", data.jwt, {
        maxAge: 30 * 24 * 60 * 60,
        path: "/",
      });

      setSuccess(true);
      setModal(false);
      router.reload();
    } catch (err) {
      setError("Something went wrong " + err);
    }
  };
  const responseGoogle = async (response) => {
    await axiosInstance
      .get(
        `/auth/google/callback?${response.tokenObj.id_token}&access_token=${response.tokenObj.access_token}`
      )
      .then((r) => {
        if (r.status === 200) {
          setModal(false);
          setCookie(null, "token", r.data["jwt"], {
            maxAge: 30 * 24 * 60 * 60,
            path: "/",
          });
          router.reload();
        }
      })
      .catch((e) => console.log(e));
  };
  return (
    <div className="card-body ">
      <h3 className="card-title text-center mb-3">Sign in</h3>
      <GoogleLogin
        clientId={process.env.CLIENT_ID}
        onSuccess={responseGoogle}
        render={(renderProps) => (
          <button
            onClick={renderProps.onClick}
            className="btn btn-google btn-block mb-4"
          >
            <FaGoogle /> Sign in with Google
          </button>
        )}
      />

      <form>
        <div className="form-group">
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <FaEnvelope />
              </span>
            </div>
            <input
              name=""
              className="form-control"
              placeholder="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className="form-group">
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <FaLock />
              </span>
            </div>
            <input
              name=""
              className="form-control"
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        {/* <div className="form-group">
          <label className="custom-control custom-checkbox">
            <input type="checkbox" className="custom-control-input" />
            <div className="custom-control-label"> Remember </div>
          </label>
        </div> */}
        <div className="form-group">
          <button
            onClick={handleSubmit}
            type="submit"
            className="btn btn-primary btn-block"
          >
            Sign In
          </button>
          {!success && error && (
            <div className="alert alert-danger mt-4" role="alert">
              {error}
            </div>
          )}
          {success && (
            <div className="alert alert-success mt-4" role="alert">
              Success
            </div>
          )}
        </div>
      </form>
    </div>
  );
}

export default Signin;
