const withPlugins = require("next-compose-plugins");
const optimizedImages = require("next-optimized-images");

const nextConfig = {
  env: {
    API_URL: "https://cms.implantreviews.com",
    CLIENT_ID:
      "912574563071-hd06bu3nur6d9f1lkf6ni6q71qoksmrh.apps.googleusercontent.com",
    NEXT_PUBLIC_GA_ID: "G-79EGCPJVZ8",
  },
};

module.exports = withPlugins([[optimizedImages, {}], nextConfig]);
